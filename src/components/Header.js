import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {withStyles} from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import ArrowBack from '@material-ui/icons/ArrowBack'
import Check from '@material-ui/icons/Check'
import yellow from '@material-ui/core/colors/yellow'
import Delete from '@material-ui/icons/Delete'
import grey from '@material-ui/core/es/colors/grey'
import {withRouter} from 'react-router-dom'

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    fabButton: {
        color: theme.palette.getContrastText(yellow[500]),
        backgroundColor: yellow[500],
        '&:hover': {
            backgroundColor: yellow[700],
        },
    },
    addButton: {
        position: 'absolute',
        right: 30,
        bottom: 30,
    },
    fadeBar: {
        backgroundColor: grey[500],
    },
})

class Header extends Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        onDelete: PropTypes.func,
        onCancel: PropTypes.func,
        onSubmit: PropTypes.func,
        title: PropTypes.string,
        backLink: PropTypes.string,
        isFade: PropTypes.bool,
        //from withRouter
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    }

    render() {
        const {classes, isFade} = this.props
        const fadeBar = isFade ? classes.fadeBar : null

        return (
            <div className={classes.root}>
                <AppBar position='static' className={fadeBar}>
                    <Toolbar>
                        {this.getLeftButton()}
                        {this.getTitle()}
                        {this.getRightButton()}
                    </Toolbar>
                </AppBar>
            </div>
        )
    }

    /**
     * Возвращает заголовок
     */
    getTitle() {
        const {classes, title} = this.props

        return (
            <Typography variant='title' color='inherit' className={classes.flex}>
                {title}
            </Typography>
        )
    }

    /**
     * Возвращает левую кнопку
     */
    getLeftButton() {
        const {classes, onCancel, backLink} = this.props

        const cancelButton = onCancel ? <IconButton
            className={classes.menuButton}
            color='inherit'
            type='submit'
            form='data'
            onClick={onCancel}>
            <ArrowBack/>
        </IconButton> : null

        const backButton = backLink ? <IconButton
            className={classes.menuButton}
            color='inherit'
            type='submit'
            form='data'
            onClick={this.goBack}>
            <ArrowBack/>
        </IconButton> : null

        const menuButton = <IconButton
            className={classes.menuButton}
            color='inherit'
            aria-label='Menu'>
            <MenuIcon/>
        </IconButton>

        return cancelButton || backButton || menuButton
    }

    /**
     * Возвращает правую кнопку
     */
    getRightButton() {
        const {onDelete, onSubmit} = this.props

        const deleteButton = onDelete ? <IconButton
            color='inherit'
            type='submit'
            form='data'
            onClick={onDelete}>
            <Delete/>
        </IconButton> : null

        const submitButton = onSubmit ? <IconButton
            color='inherit'
            type='submit'
            onClick={onSubmit}>
            <Check/>
        </IconButton> : null

        return deleteButton || submitButton || null
    }

    goBack = () => {
        const {history, backLink} = this.props
        history.push(backLink)
    }
}

export default withStyles(styles)(withRouter(Header))