import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {deleteNote, loadAllNotes} from '../../ac/index'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import withSelectableItems from '../../decorators/withSelectableItems'
import {mapToArr} from '../../helpers'

import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import yellow from '@material-ui/core/colors/yellow'

import Header from '../Header'
import NoteList from '../NoteList'

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    fabButton: {
        color: theme.palette.getContrastText(yellow[500]),
        backgroundColor: yellow[500],
        '&:hover': {
            backgroundColor: yellow[700],
        },
    },
    addButton: {
        position: 'absolute',
        right: 30,
        bottom: 30,
    },
})

class Notes extends Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        //from connect
        notes: PropTypes.array.isRequired,
        deleteNote: PropTypes.func.isRequired,
        loadAllNotes: PropTypes.func.isRequired,
        loading: PropTypes.bool.isRequired,
        //from withSelectableItems
        selectedItems: PropTypes.array.isRequired,
        onSelect: PropTypes.func.isRequired,
        onCleanSelectedItems: PropTypes.func.isRequired,
    }

    componentDidMount() {
        const {loadAllNotes} = this.props
        loadAllNotes()
    }

    render() {
        const {selectedItems, onSelect, notes, loading, classes, onCleanSelectedItems} = this.props
        const handleCancel = selectedItems.length ? onCleanSelectedItems : undefined
        const handleDelete = selectedItems.length ? this.handleDelete : undefined

        return (
            <div className={classes.root}>
                <Header
                    onDelete={handleDelete}
                    onCancel={handleCancel}
                    title={this.getTitle()}
                    isFade={!!selectedItems.length}
                />
                <NoteList
                    notes={notes}
                    loading={loading}
                    selectedNotes={selectedItems}
                    onSelect={onSelect}
                    onEdit={this.handleEdit}
                />
                <Link to='/add' className={classes.addButton}>
                    <Button variant='fab' color='secondary' className={classes.fabButton}>
                        <AddIcon/>
                    </Button>
                </Link>
            </div>
        )
    }

    /**
     * Возвращает заголовок для шапки
     */
    getTitle() {
        const {selectedItems} = this.props
        return selectedItems.length ? selectedItems.length.toString() : 'Заметки'
    }

    /**
     * Callback для удаления выбранных заметок
     */
    handleDelete = () => {
        const {deleteNote, selectedItems, onCleanSelectedItems} = this.props
        selectedItems.forEach(id => deleteNote(id))
        onCleanSelectedItems()
    }

    /**
     * Callback для редатирования заметки
     */
    handleEdit = (id) => {
        const {history} = this.props
        history.push('/edit/' + id)
    }
}

const decorator = connect(state => ({
    notes: mapToArr(state.notes.entities),
    loading: state.notes.loading,
}), {deleteNote, loadAllNotes})

export default withStyles(styles)(decorator(withSelectableItems(Notes)))