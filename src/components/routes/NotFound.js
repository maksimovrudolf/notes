import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Grid from '@material-ui/core/Grid'

class NotFound extends Component {
    render() {
        return (
            <Grid container>
                <Grid item xs={12}>
                    <div style={{textAlign: 'center'}}>
                        <h3>NotFound</h3>
                        <div>
                            <Link to='/'>На главную</Link>
                        </div>
                    </div>
                </Grid>
            </Grid>
        )
    }
}

export default NotFound