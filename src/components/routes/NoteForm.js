import React, {Component} from 'react'
import {connect} from 'react-redux'
import {editNote, addNote} from '../../ac/index'
import PropTypes from 'prop-types'
import Header from '../Header'

import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import {withStyles} from '@material-ui/core'

const styles = theme => ({
    form: {
        display: 'block',
        maxWidth: '600px',
        margin: 'auto',
    }
})

class NoteForm extends Component {
    static propTypes = {
        //from connect
        note: PropTypes.shape({
            text: PropTypes.string,
            id: PropTypes.string
        }),
        //from Route
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired,
    }

    constructor(props) {
        super(props)

        this.id = null
        let text = ''

        const {note} = this.props

        if (note.id) {
            this.id = note.id
            text = note.text
        }

        this.state = {
            text,
            errors: {
                text: null
            },
        }

        this.textInput = React.createRef()
    }

    render() {
        const {classes} = this.props

        return (
            <React.Fragment>
                <Header
                    title={this.getTitle()}
                    onSubmit={this.handleSubmit}
                    backLink='/'
                />
                <Grid container>
                    <Grid item xs={12}>
                        <form className={classes.form}>
                            <TextField
                                margin='normal'
                                value={this.state.text}
                                onChange={this.handleChange('text')}
                                fullWidth
                                multiline
                                autoFocus
                                inputRef={this.textInput}
                                helperText={this.state.errors.text}
                            />
                        </form>
                    </Grid>
                </Grid>
            </React.Fragment>
        )
    }

    /**
     * Возвращает заголовок для шапки
     */
    getTitle() {
        return (this.props.note.id) ? 'Редактировать заметку' : 'Добавить заметку'
    }

    /**
     * Callback для текстовых полей, который заносит value в state
     */
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
            errors: {
                [name]: null
            },
        })
    }

    /**
     * Callback для сохранения заметки
     */
    handleSubmit = () => {
        let errors = {}

        if (!this.state.text.trim()) {
            errors.text = 'Заметка не может быть пустой'
        }

        if (errors.text) {
            this.textInput.current.focus()
            this.setState({errors})
            return
        }

        const {editNote, addNote, history} = this.props

        if (this.id) {
            editNote({
                text: this.state.text.trim(),
                id: this.id
            })
        } else {
            addNote({
                releaseDate: Date.now().toString(),
                text: this.state.text.trim(),
            })
        }

        history.push('/')
    }
}

const decorator = connect(({notes}, {match, history}) => {
    let note = {}

    if (match.params.id) {
        note = notes.entities.get(match.params.id) || {}

        if (!note.id) {
            history.replace('/')
        }
    }

    return {note}
}, {editNote, addNote})

export default withStyles(styles)(decorator(NoteForm))