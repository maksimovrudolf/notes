import React from 'react'
import Note from './Note'
import PropTypes from 'prop-types'

import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid'
import {withStyles} from '@material-ui/core'

function NoteList(props) {
    const {selectedNotes, onSelect, notes, loading, onEdit, classes} = props

    if (loading) {
        return <Grid container justify='center'>
            <CircularProgress className={classes.progress}/>
        </Grid>
    } else {
        return notes.map(note => <Note
            note={note}
            key={note.id}
            id={note.id}
            isSelected={(selectedNotes.indexOf(note.id) !== -1)}
            isPress={!selectedNotes.length}
            onSelect={onSelect}
            onEdit={onEdit}
        />)
    }
}

NoteList.propTypes = {
    notes: PropTypes.array.isRequired,
    selectedNotes: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    onSelect: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    //from withStyles
    classes: PropTypes.object.isRequired,
}

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
})

export default withStyles(styles)(NoteList)