import React from 'react'
import PropTypes from 'prop-types'
import withSelectable from '../decorators/withSelectable'
import {getDateString} from '../helpers'

import {withStyles} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import grey from '@material-ui/core/es/colors/grey'

function Note(props) {
    const {note, classes, isSelected, onTouchStart, onTouchEnd, onMouseDown, onMouseUp, onClick} = props
    const selectedClass = isSelected ? classes.selected : null
    const paperClass = [classes.root, selectedClass].join(' ')

    return (
        <Paper
            className={paperClass}
            elevation={1}
            onTouchStart={onTouchStart}
            onTouchEnd={onTouchEnd}
            onMouseDown={onMouseDown}
            onMouseUp={onMouseUp}
            onClick={onClick}
        >
            <Typography variant='headline' component='h3'>
                {note.text}
            </Typography>
            <Typography component='p'>
                {getDateString(note.releaseDate)}
            </Typography>
        </Paper>
    )
}

Note.propTypes = {
    note: PropTypes.shape({
        text: PropTypes.string,
        id: PropTypes.string,
        releaseDate: PropTypes.string,
    }).isRequired,
    id: PropTypes.string.isRequired,
    isSelected: PropTypes.bool.isRequired,
    isPress: PropTypes.bool.isRequired,
    onSelect: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    //from withSelectable
    onTouchStart: PropTypes.func.isRequired,
    onTouchEnd: PropTypes.func.isRequired,
    onMouseDown: PropTypes.func.isRequired,
    onMouseUp: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    //from withStyles
    classes: PropTypes.object.isRequired,
}

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        maxWidth: '600px',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '30px',
        userSelect: 'none',
    },
    selected: {
        backgroundColor: grey[300],
    },
})

export default withStyles(styles)(withSelectable(Note))