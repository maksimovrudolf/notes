import React, {Component} from 'react'
import Notes from './routes/Notes'
import NoteForm from './routes/NoteForm'
import NotFound from './routes/NotFound'
import {Provider} from 'react-redux'
import store from '../store'
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <React.Fragment>
                        <CssBaseline/>
                        <Switch>
                            <Route path='/' component={Notes} exact></Route>
                            <Route path='/add' component={NoteForm}></Route>
                            <Route path='/edit/:id' component={NoteForm}></Route>
                            <Redirect from='/edit/' to='/'/>
                            <Route path='*' component={NotFound}/>
                        </Switch>
                    </React.Fragment>
                </Router>
            </Provider>
        )
    }
}

export default App