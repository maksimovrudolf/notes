import {DELETE_NOTE, ADD_NOTE, EDIT_NOTE, LOAD_ALL_NOTES} from '../actionTypes'

export function deleteNote(id) {
    return {
        type: DELETE_NOTE,
        payload: {id},
    }
}

export function addNote(note) {
    return {
        type: ADD_NOTE,
        payload: note,
        generateId: true
    }
}

export function editNote(note) {
    return {
        type: EDIT_NOTE,
        payload: note,
    }
}

export function loadAllNotes() {
    return {
        type: LOAD_ALL_NOTES,
        callAPI: '/api/notes.json'
    }
}