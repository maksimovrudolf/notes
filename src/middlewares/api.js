import {START, SUCCESS, FAIL} from '../actionTypes'

export default store => next => action => {
    const {callAPI, type, ...rest} = action

    if (!callAPI) return next(action)

    const {notes: {loading, loaded}} = store.getState()

    if (loading || loaded) return

    next({
        ...rest, type: type + START
    })

    //для имитации загрузки
    setTimeout(() => {
        fetch(callAPI)
            .then(res => res.json())
            .then(response => next({...rest, type: type + SUCCESS, response}))
            .catch(error => next({...rest, type: type + FAIL, error}))
    }, 500)
}