import {OrderedMap, Map} from 'immutable'

export function arrToMap(arr, DataRecord = Map) {
    return arr.reduce((acc, item) => acc.set(item.id, new DataRecord(item)), new OrderedMap({}))
}

export function mapToArr(map) {
    return map.valueSeq().toArray()
}

export function getDateString(milliseconds) {
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        weekday: 'long',
        timezone: 'UTC',
    }

    let dateString = new Date(+milliseconds).toLocaleString('ru', options)
    dateString = dateString[0].toUpperCase() + dateString.slice(1)
    dateString = dateString.substring(0, dateString.length - 3)

    return dateString
}