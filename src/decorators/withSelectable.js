import React, {Component} from 'react'

export default (OriginalComponent) => {
    class withSelectable extends Component {
        state = {
            isClick: true
        }

        render() {
            return (<OriginalComponent
                {...this.props}
                {...this.state}
                onTouchStart={this.handlePress}
                onTouchEnd={this.handleRelease}
                onMouseDown={this.handlePress}
                onMouseUp={this.handleRelease}
                onClick={this.handleClick}
            />)
        }

        /**
         * Callback для нажатия, который выделяет элемент при удерживании
         */
        handlePress = () => {
            const {isPress, onSelect} = this.props

            if (isPress) {
                this.PressTimer = setTimeout(() => {
                    onSelect(this.props.id)
                    //запрещаем клик после удерживания, чтобы после не сработал handleClick
                    this.setState({isClick: false})
                }, 500)
            } else {
                this.setState({
                    isClick: true
                })
            }
        }

        /**
         * Callback для отжатия, который очищает таймер, запущенный в handlePress
         */
        handleRelease = () => {
            const {isPress} = this.props

            if (isPress) clearTimeout(this.PressTimer)
        }

        /**
         * Callback для клика, который либо выделяет элемент, либо переходит в режим редактирования
         */
        handleClick = () => {
            const {isPress, onSelect, id, onEdit} = this.props

            if (!isPress) {
                if (this.state.isClick) {
                    onSelect(id)
                }
            } else {
                onEdit(id)
            }
        }
    }

    withSelectable.displayName =
        `withSelectable(${OriginalComponent.displayName || OriginalComponent.name || 'Component'})`

    return withSelectable
}