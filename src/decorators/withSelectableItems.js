import React, {Component} from 'react'

export default (OriginalComponent) => {
    class withSelectableItems extends Component {
        state = {
            selectedItems: []
        }

        render() {
            return (<OriginalComponent
                {...this.props}
                {...this.state}
                onSelect={this.handleSelect}
                onCleanSelectedItems={this.handleClean}
                selectedItems={this.state.selectedItems}
            />)
        }

        /**
         * Callback для выбора элементов
         */
        handleSelect = (id) => {
            const selectedItems = this.state.selectedItems

            if (selectedItems.indexOf(id) === -1) {
                this.setState({
                    selectedItems: selectedItems.concat(id)
                })
            } else {
                this.setState({
                    selectedItems: selectedItems.filter(value => value !== id)
                })
            }
        }

        /**
         * Очистка выбранных элементов
         */
        handleClean = () => {
            this.setState({
                selectedItems: []
            })
        }
    }

    withSelectableItems.displayName =
        `withSelectableItems(${OriginalComponent.displayName || OriginalComponent.name || 'Component'})`

    return withSelectableItems
}