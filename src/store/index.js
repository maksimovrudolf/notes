import {createStore, applyMiddleware } from 'redux'
import reducer from '../reducer'
//import logger from '../middlewares/logger'
import randomid from '../middlewares/randomid'
import api from '../middlewares/api'

const enhancer = applyMiddleware(randomid, api/*, logger*/)

const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    enhancer
)

export default store