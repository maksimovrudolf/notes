import {combineReducers} from 'redux'
import notes from './notes'

export default combineReducers({
    notes
})

// export default function cr(state = {}, action) {
//     return {
//         notes: notes(state.notes, action)
//     }
// }