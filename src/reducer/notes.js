import {DELETE_NOTE, ADD_NOTE, EDIT_NOTE, LOAD_ALL_NOTES, START, SUCCESS} from '../actionTypes'
import {arrToMap} from '../helpers'
import {OrderedMap, Record} from 'immutable'

const NoteRecord = Record({
    id: undefined,
    releaseDate: undefined,
    text: undefined
})

const ReducerState = Record({
    loading: false,
    loaded: false,
    entities: new OrderedMap({})
})

const defaultState = new ReducerState()

export default (notesState = defaultState, action) => {
    const {type, payload, randomId, response} = action

    switch (type) {
        case DELETE_NOTE:
            return notesState.deleteIn(['entities', payload.id])
        case ADD_NOTE:
            return notesState.setIn(['entities', randomId], new NoteRecord({id: randomId, ...payload}))
        case EDIT_NOTE:
            return notesState.updateIn(['entities', payload.id, 'text'], text => payload.text)
        case LOAD_ALL_NOTES + START:
            return notesState.set('loading', true)
        case LOAD_ALL_NOTES + SUCCESS:
            return notesState
                .set('entities', arrToMap(response, NoteRecord))
                .set('loading', false)
                .set('loaded', true)
        default:
            return notesState
    }
}