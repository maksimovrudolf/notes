export const DELETE_NOTE = 'DELETE_NOTE'
export const ADD_NOTE = 'ADD_NOTE'
export const EDIT_NOTE = 'EDIT_NOTE'
export const LOAD_ALL_NOTES = 'LOAD_ALL_NOTES'

export const START = '_START'
export const SUCCESS = '_SUCCESS'
export const FAIL = '_FAIL'